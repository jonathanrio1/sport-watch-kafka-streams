#################################################################################################################################
# The heart, temperature and oxymeter metrics are 0 or low for a story purpose concerning a non human wearing a connected watch #
#                                           WELCOME TO THE WORLD OF THE AUTHOR KAFKA !                                          #
#################################################################################################################################

import psycopg2, time

update_query = """update samsacorp.connected_watch set steps=%s, distance=%s, heart=%s, temperature=%s, oxymeter=%s, 
    altimeter=%s, pressure=%s, gpslat=%s, gpslong=%s, battery=%s where memberid = 3 and date = current_date"""


def update_records(records, cursor, connection):
    cursor.execute(update_query, records)
    connection.commit()


try:
    connection = psycopg2.connect(user="postgres", password="postgres", host="localhost", port="5432",
                                  database="postgres")
    cursor = connection.cursor()

    steps = 0
    distance = 0
    heart = 0
    temperature = 19.2
    oxymeter = 15
    altimeter = 10
    pressure = 1.01325
    gpslat = 47.2612864
    gpslong = -0.0720896
    battery = 100

    count = 0

    # activity = rest
    for x in range(10):
        battery -= 0.1

        record_to_update = (steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery)
        update_records(record_to_update, cursor, connection)

        count += 1
        print(count, "'having rest...")
        time.sleep(1)

    # activity = moderate sport
    for x in range(15):
        steps += 1
        distance += 0.01
        temperature = 20.5
        oxymeter = 17
        gpslat += 0.0000001
        gpslong += 0.0000001
        battery -= 0.1

        record_to_update = (steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery)
        update_records(record_to_update, cursor, connection)

        count += 1
        print(count, "low physical activity...")
        time.sleep(1)

    # activity = high sport
    for x in range(15):
        steps += 1
        distance += 0.02
        temperature = 22.1
        oxymeter = 19
        gpslat += 0.0000002
        gpslong += 0.0000002
        battery -= 0.1

        record_to_update = (steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery)
        update_records(record_to_update, cursor, connection)

        count += 1
        print(count, "'high physical activity...")
        time.sleep(1)

    # activity = rest
    for x in range(10):
        temperature = 20.1
        oxymeter = 16
        battery -= 0.1

        record_to_update = (steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery)
        update_records(record_to_update, cursor, connection)

        count += 1
        print(count, "'having rest...")
        time.sleep(1)


except (Exception, psycopg2.Error) as error:
    print("Failed to update connected watch datas", error)

finally:
    # closing database connection.
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
