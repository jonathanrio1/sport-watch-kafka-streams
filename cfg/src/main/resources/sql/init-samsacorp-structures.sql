-- Nettoyage au préalable
REVOKE ALL PRIVILEGES ON SCHEMA samsacorp FROM debezium;
REVOKE ALL PRIVILEGES ON TABLE samsacorp.connected_watch FROM debezium;
REVOKE ALL PRIVILEGES ON SEQUENCE samsacorp.connected_watch_id_seq FROM debezium;
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA samsacorp FROM debezium;

REASSIGN OWNED BY debezium TO postgres;
DROP OWNED BY debezium;

DROP PUBLICATION IF EXISTS dbz_publication;

DROP USER IF EXISTS debezium;

CREATE USER debezium WITH PASSWORD 'debezium';
ALTER USER debezium REPLICATION LOGIN;
GRANT ALL ON SCHEMA samsacorp TO debezium;

-- Création des tables et permissions
DROP TABLE IF EXISTS samsacorp.risk_rates;
CREATE TABLE IF NOT EXISTS samsacorp.risk_rates
(
    id serial primary key,
    risk integer not null,
    price double precision not null
);

ALTER TABLE samsacorp.risk_rates OWNER TO debezium;
GRANT ALL ON TABLE samsacorp.risk_rates TO debezium;
GRANT ALL ON SEQUENCE samsacorp.risk_rates_id_seq TO debezium;

insert into samsacorp.risk_rates (risk, price) values
(1, 0.001), (2, 0.01), (3, 0.02);

DROP TABLE IF EXISTS samsacorp.members;
CREATE TABLE IF NOT EXISTS samsacorp.members
(
    id serial primary key,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    age integer not null,
    risk double precision not null default 0,
    fee double precision not null default 0
);

ALTER TABLE samsacorp.members OWNER TO debezium;
GRANT ALL ON TABLE samsacorp.members TO debezium;
GRANT ALL ON SEQUENCE samsacorp.members_id_seq TO debezium;

DROP TABLE IF EXISTS samsacorp.connected_watch;
CREATE TABLE IF NOT EXISTS samsacorp.connected_watch
(
    id serial primary key,
    memberId integer not null,
    date date not null,
    steps integer,
    distance double precision,
    heart integer,
    temperature double precision,
    oxymeter integer,
    altimeter double precision,
    pressure double precision,
    gpsLat double precision,
    gpsLong double precision,
    battery double precision,
    CONSTRAINT connected_watch_unique UNIQUE (memberid, date)
);

ALTER TABLE samsacorp.connected_watch OWNER TO debezium;
GRANT ALL ON TABLE samsacorp.connected_watch TO debezium;
GRANT ALL ON SEQUENCE samsacorp.connected_watch_id_seq TO debezium;

-- Initialisation des données
INSERT INTO samsacorp.members (firstname, lastname, age, risk, fee) VALUES ('Gregor', 'Doe', 36, 1.25, 0.76);
INSERT INTO samsacorp.members (firstname, lastname, age, risk, fee) VALUES ('Samantha', 'Sanders', 37, 1.15, 0.72);
INSERT INTO samsacorp.members (firstname, lastname, age, risk, fee) VALUES ('Damon', 'Dusk', 52, 0, 0);

INSERT INTO samsacorp.connected_watch(memberId, date, steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery) VALUES (1, CURRENT_DATE - 1, 82, 1.10, 83, 38.1, 98, 10, 1.01325, 47.260135, -0.080893, 96);
INSERT INTO samsacorp.connected_watch(memberId, date, steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery) VALUES (2, CURRENT_DATE - 1, 96, 1.10, 75, 37.7, 97, 10, 1.01325, 47.260135, -0.080893, 95);
INSERT INTO samsacorp.connected_watch(memberId, date, steps, distance, heart, temperature, oxymeter, altimeter, pressure, gpslat, gpslong, battery) VALUES (3, CURRENT_DATE, null, null, null, null, null, null, null, null, null, null);

-- Suppression du slot de replication s'il existe
SELECT pg_drop_replication_slot('debezium')
WHERE EXISTS (SELECT 1 from pg_replication_slots where slot_name = 'debezium');

-- Création de la publication pour le connecteur source
CREATE PUBLICATION dbz_publication FOR TABLE samsacorp.connected_watch;
ALTER PUBLICATION dbz_publication OWNER TO debezium;

-- Le décodage 'logical' est un pré-requis pour le Change Data Capture
ALTER SYSTEM SET wal_level = logical;
-- Pour que les événements UPDATE et DELETE contiennent les valeurs précédentes
ALTER TABLE samsacorp.connected_watch REPLICA IDENTITY FULL;