   _____ ____  ____  ____  ______   _       _____  ______________  __
  / ___// __ \/ __ \/ __ \/_  __/  | |     / /   |/_  __/ ____/ / / /
  \__ \/ /_/ / / / / /_/ / / /     | | /| / / /| | / / / /   / /_/ /
 ___/ / ____/ /_/ / _, _/ / /      | |/ |/ / ___ |/ / / /___/ __  /
/____/_/    \____/_/ |_| /_/       |__/|__/_/  |_/_/  \____/_/ /_/


${Ansi.GREEN}Application : ${application.title}${application.formatted-version} - Using Spring Boot${spring-boot.formatted-version}
