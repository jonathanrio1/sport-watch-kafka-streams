@echo off
cd C:\dev\tools\kafka

echo "Recreating the logs folders..."
for %%d in (".\logs" ".\kafka-logs" ".\zookeeper-data") do @rmdir "%%~d" /s /q
md logs kafka-logs zookeeper-data

echo "Starting Zookeeper..."
start cmd /k .\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties
timeout 3

echo "Starting Kafka..."
start cmd /k .\bin\windows\kafka-server-start.bat .\config\server.properties
timeout 6

echo "Creating the topics..."
call .\bin\windows\kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic postgres.samsacorp.connected_watch
call .\bin\windows\kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic members
call .\bin\windows\kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic events_error
call .\bin\windows\kafka-topics.bat --list --bootstrap-server localhost:9092

echo "Starting Kafka Connect..."
start cmd /k .\bin\windows\connect-standalone.bat .\config\connect-standalone.properties .\config\postgres-samsacorp-source-connector.properties .\config\postgres-samsacorp-sink-connector.properties
PAUSE