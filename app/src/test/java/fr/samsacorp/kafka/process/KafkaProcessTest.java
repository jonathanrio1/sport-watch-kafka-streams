package fr.samsacorp.kafka.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.samsacorp.kafka.model.TopicKey;
import fr.samsacorp.kafka.model.TopicValue;
import fr.samsacorp.kafka.util.KeyWrapperSerde;
import fr.samsacorp.kafka.util.ValueWrapperSerde;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class KafkaProcessTest {

    private static ObjectMapper objectMapper;
    private static final String INPUT_TOPIC = "input_topic";
    private static final String OUTPUT_TOPIC = "output_topic";

    @BeforeAll
    static void init() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    void processFromDatabase() throws IOException {

        TopicKey key = objectMapper.readValue(
                new File(this.getClass().getClassLoader().getResource("TopicKey.json").getFile()),
                TopicKey.class);

        TopicValue value = objectMapper.readValue(
                new File(this.getClass().getClassLoader().getResource("TopicValue.json").getFile()),
                TopicValue.class);

        StreamsBuilder builder = new StreamsBuilder();
        builder.stream(INPUT_TOPIC).to(OUTPUT_TOPIC);
        Topology topology = builder.build();

        Properties config = new Properties();
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, KeyWrapperSerde.class.getName());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, ValueWrapperSerde.class.getName());

        TopologyTestDriver testDriver = new TopologyTestDriver(topology, config);

        TestInputTopic<TopicKey, TopicValue> inputTopic = testDriver.createInputTopic(
                INPUT_TOPIC, new JsonSerializer<>(), new JsonSerializer<>());

        inputTopic.pipeInput(key, value);

        TestOutputTopic<TopicKey, TopicValue> outputTopic = testDriver.createOutputTopic(
                OUTPUT_TOPIC, new JsonDeserializer<>(TopicKey.class),
                new JsonDeserializer<>(TopicValue.class));

        TopicValue outputValue = outputTopic.readValue();

        assertAll(() -> {
            assertEquals("c", outputValue.getPayload().getOp());
            assertEquals(1, outputValue.getPayload().getAfter().getConnectedWatchFields().getId());
            assertTrue(outputTopic.isEmpty());
        });
    }
}