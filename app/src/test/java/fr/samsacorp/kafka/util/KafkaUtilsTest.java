package fr.samsacorp.kafka.util;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertEquals;

class KafkaUtilsTest {

    @Test
    void getHourFromTime() {
        int hour = KafkaUtils.getHourFromTime(26460000);
        assertEquals(7, hour);
    }

    @Test
    void decodeDecimal() {
        BigDecimal expected = new BigDecimal("1.00");
        final BigDecimal decoded = new BigDecimal(new BigInteger(Base64.getDecoder().decode("ZA==")), 2);
        assertEquals(expected, decoded);
    }
}