package fr.samsacorp.kafka.util;

import fr.samsacorp.kafka.model.TopicKey;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class KeyWrapperSerde extends Serdes.WrapperSerde<TopicKey> {

    public KeyWrapperSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(TopicKey.class));
    }

}
