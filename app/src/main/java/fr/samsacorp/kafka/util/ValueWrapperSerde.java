package fr.samsacorp.kafka.util;

import fr.samsacorp.kafka.model.TopicValue;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class ValueWrapperSerde extends Serdes.WrapperSerde<TopicValue> {

    public ValueWrapperSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(TopicValue.class));
    }

}
