package fr.samsacorp.kafka.util;

import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.ZoneOffset;


@Slf4j
public class KafkaUtils {

    private KafkaUtils() {
        throw new UnsupportedOperationException("That utility class can not be instantiated");
    }

    public static boolean isBetween(int toCheck, int from, int to){
        return from <= toCheck && toCheck <= to;
    }

    public static int getHourFromTime(int time) {
        return Instant.ofEpochMilli(time).atZone(ZoneOffset.UTC).getHour();
    }
}
