package fr.samsacorp.kafka.process;

import fr.samsacorp.entity.MembersEntity;
import fr.samsacorp.entity.RiskRatesEntity;
import fr.samsacorp.kafka.model.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static fr.samsacorp.kafka.util.KafkaUtils.isBetween;

public class KafkaCalculation {

    private KafkaCalculation() {
        throw new UnsupportedOperationException("That calculation class can not be instantiated");
    }

    private static final String BEFORE = "before";
    private static final String AFTER = "after";
    private static final String STRING_TYPE = "string";
    private static final String INTEGER_TYPE = "int32";
    private static final String DOUBLE_TYPE = "double";

    public static void buildDataForMember(TopicKey key, TopicValue value, RiskRatesEntity riskRates,
                                          MembersEntity member) {

        value.getPayload().setOp("u");
        key.getPayload().setId(member.getId());

        double dailyFee = member.getFee() + riskRates.getPrice();
        double dailyRisk = member.getRisk() != 0 ? (member.getRisk() + riskRates.getRisk()) / 2 : riskRates.getRisk();

        value.getPayload().setBefore(BeforeAfterFields.builder()
                .memberFields(MemberFields.builder()
                        .id(member.getId())
                        .firstname(member.getFirstname())
                        .lastname(member.getLastname())
                        .age(member.getAge())
                        .risk(member.getRisk())
                        .fee(member.getFee())
                        .build())
                .build());

        value.getPayload().setAfter(BeforeAfterFields.builder()
                .memberFields(MemberFields.builder()
                        .id(member.getId())
                        .firstname(member.getFirstname())
                        .lastname(member.getLastname())
                        .age(member.getAge())
                        .risk(dailyRisk)
                        .fee(dailyFee)
                        .build())
                .build());

        value.getSchema().getFields().stream()
                .filter(f -> BEFORE.equals(f.getField()))
                .findFirst().ifPresent(KafkaCalculation::buildSchemaForMember);

        value.getSchema().getFields().stream()
                .filter(f -> AFTER.equals(f.getField()))
                .findFirst().ifPresent(KafkaCalculation::buildSchemaForMember);
    }

    private static void buildSchemaForMember(TopicKeyFields field) {
        List<TopicKeyFields> fields = field.getFields().stream().filter(f ->
                !Arrays.asList("date", "memberid", "steps", "distance", "heart", "temperature", "oxymeter",
                        "altimeter", "pressure", "gpslat", "gpslong", "battery")
                        .contains(f.getField())).collect(Collectors.toList());
        fields.addAll(Arrays.asList(
                TopicKeyFields.builder().field("firstname").optional(false).type(STRING_TYPE).build(),
                TopicKeyFields.builder().field("lastname").optional(false).type(STRING_TYPE).build(),
                TopicKeyFields.builder().field("age").optional(false).type(INTEGER_TYPE).build(),
                TopicKeyFields.builder().field("risk").optional(true).type(DOUBLE_TYPE).build(),
                TopicKeyFields.builder().field("fee").optional(true).type(DOUBLE_TYPE).build()
        ));
        field.setFields(fields);
    }

    public static int estimateRisk(ConnectedWatchFields connectedWatchData) {
        int heartRisk = connectedWatchData.getHeart() != null ?
                heartRisk(connectedWatchData.getHeart()) : 1;
        int temperatureRisk = connectedWatchData.getTemperature() != null ?
                temperatureRisk(connectedWatchData.getTemperature()) : 1;
        int oxymeterRisk = connectedWatchData.getOxymeter() != null ?
                oxymeterRisk(connectedWatchData.getOxymeter()) : 1;
        int pressureRisk = connectedWatchData.getPressure() != null ?
                pressureRisk(connectedWatchData.getPressure()) : 1;

        List<Integer> risks = Arrays.asList(heartRisk, temperatureRisk, oxymeterRisk, pressureRisk);
        return risks.contains(3) ? 3 : risks.contains(2) ? 2 : 1;
    }

    /**
     *
     * @param heart -50 high, 50-59 moderate, 60-80 low, 81-110 moderate, 110+ high
     * @return the estimated risk
     */
    private static int heartRisk(int heart) {
        if (isBetween(heart, 60, 80)) {
            // low
            return 1;
        } else if (isBetween(heart, 50, 59) || isBetween(heart, 81, 110)) {
            // moderate
            return 2;
        } else {
            // high
            return 3;
        }
    }

    /**
     *
     * @param temperature -35 high, 35-36 moderate, 37-38 low, 39-40 moderate, 40+ high
     * @return the estimated risk
     */
    private static int temperatureRisk(double temperature) {
        if (isBetween((int)temperature, 37, 38)) {
            // low
            return 1;
        } else if (isBetween((int)temperature, 35, 36) || isBetween((int)temperature, 39, 40)) {
            // moderate
            return 2;
        } else {
            // high
            return 3;
        }
    }

    /**
     *
     * @param oxymeter -90 high, 90-94 moderate, 95-100 low
     * @return the estimated risk
     */
    private static int oxymeterRisk(int oxymeter) {
        if (isBetween(oxymeter, 95, 100)) {
            // low
            return 1;
        } else if (isBetween(oxymeter, 90, 94)) {
            // moderate
            return 2;
        } else {
            // high
            return 3;
        }
    }

    /**
     *
     * @param pressure -3 high, 2-3 moderate, 0-1 low
     * @return the estimated risk
     */
    private static int pressureRisk(double pressure) {
        if (isBetween((int)pressure, 0, 1)) {
            // low
            return 1;
        } else if (isBetween((int)pressure, 2, 3)) {
            // moderate
            return 2;
        } else {
            // high
            return 3;
        }
    }

}
