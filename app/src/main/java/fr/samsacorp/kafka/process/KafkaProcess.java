package fr.samsacorp.kafka.process;

import fr.samsacorp.entity.MembersEntity;
import fr.samsacorp.entity.RiskRatesEntity;
import fr.samsacorp.kafka.model.ConnectedWatchFields;
import fr.samsacorp.kafka.model.TopicKey;
import fr.samsacorp.kafka.model.TopicValue;
import fr.samsacorp.service.SportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static fr.samsacorp.kafka.process.KafkaCalculation.buildDataForMember;
import static fr.samsacorp.kafka.process.KafkaCalculation.estimateRisk;


@Component
@Slf4j
public class KafkaProcess {
    SportService sportService;

    public KafkaProcess(SportService sportService) {
        this.sportService = sportService;
    }

    @Value("${kafka.topic.in}")
    private String topicIn;

    @Value("${kafka.topic.out}")
    private String topicOut;

    private static final List<String> OPERATIONS = Arrays.asList("c", "u");


    @Autowired
    void process(StreamsBuilder streamsBuilder) {

        KStream<TopicKey, TopicValue> kStream = streamsBuilder.stream(topicIn);

        kStream.filter((key, value) -> value != null && key != null && OPERATIONS.contains(value.getPayload().getOp()))
                .map((key, value) -> {
                    log.debug("Incoming record : input topic = '{}' - ID = {} - value before transformation = {}",
                            topicIn, key.getPayload().getId(), value);

                    ConnectedWatchFields connectedWatchDatas = value.getPayload().getAfter().getConnectedWatchFields();
                    // Récupération des données membre
                    MembersEntity member = sportService.getMemberById(connectedWatchDatas.getMemberId());
                    // Estimation du risque
                    int risk = estimateRisk(connectedWatchDatas);
                    if (risk == 3) {
                        log.warn("### WARNING ### {} {} is risky !", member.getFirstname(), member.getLastname());
                    }
                    // Récupération du prix en fonction du risque
                    RiskRatesEntity riskRates = sportService.getPriceByRisk(risk);
                    buildDataForMember(key, value, riskRates, member);

                    log.debug("Outcoming record : output topic = '{}' - ID = {} - value after transformation = {}",
                            topicOut, key.getPayload().getId(), value);

                    return new KeyValue<>(key, value);
                })
                .to(topicOut);
    }

}
