package fr.samsacorp.kafka.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopicKeySchema {

    @JsonProperty("type")
    String type;

    @JsonProperty("fields")
    ArrayList<TopicKeyFields> fields;

    @JsonProperty("optional")
    boolean optional;

    @JsonProperty("name")
    String name;

    @JsonProperty("version")
    int version;

}
