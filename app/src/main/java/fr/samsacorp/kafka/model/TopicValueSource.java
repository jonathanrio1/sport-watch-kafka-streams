package fr.samsacorp.kafka.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopicValueSource {

    @JsonProperty("version")
    String version;

    @JsonProperty("connector")
    String connector;

    @JsonProperty("name")
    String name;

    @JsonProperty("ts_ms")
    long tsMs;

    @JsonProperty("snapshot")
    String snapshot;

    @JsonProperty("db")
    String db;

    @JsonProperty("sequence")
    String sequence;

    @JsonProperty("schema")
    String schema;

    @JsonProperty("table")
    String table;

    @JsonProperty("txId")
    int txId;

    @JsonProperty("lsn")
    int lsn;

    @JsonProperty("xmin")
    Object xmin;

}
