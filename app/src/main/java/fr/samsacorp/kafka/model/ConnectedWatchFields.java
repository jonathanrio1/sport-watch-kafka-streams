package fr.samsacorp.kafka.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConnectedWatchFields {

    @JsonProperty("id")
    int id;

    @JsonProperty("memberid")
    int memberId;
    
    @JsonProperty("date")
    Date date;
    
    @JsonProperty("steps")
    Integer steps;
    
    @JsonProperty("distance")
    Double distance;
    
    @JsonProperty("heart")
    Integer heart;
    
    @JsonProperty("temperature")
    Double temperature;

    @JsonProperty("oxymeter")
    Integer oxymeter;

    @JsonProperty("altimeter")
    Double altimeter;

    @JsonProperty("pressure")
    Double pressure;

    @JsonProperty("gpslat")
    Double gpslat;

    @JsonProperty("gpslong")
    Double gpslong;

    @JsonProperty("battery")
    Double battery;
}
