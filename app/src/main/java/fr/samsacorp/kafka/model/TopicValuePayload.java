package fr.samsacorp.kafka.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonMerge;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopicValuePayload {

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("before")
    BeforeAfterFields before;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("after")
    @JsonMerge
    BeforeAfterFields after;

    @JsonProperty("source")
    TopicValueSource source;

    @JsonProperty("op")
    String op;

    @JsonProperty("ts_ms")
    Long tsMs;

    @JsonProperty("transaction")
    Object transaction;

}
