package fr.samsacorp.kafka.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopicKeyFields {

    @JsonProperty("type")
    String type;

    @JsonProperty("fields")
    List<TopicKeyFields> fields;

    @JsonProperty("optional")
    Boolean optional;

    @JsonProperty("name")
    String name;

    @JsonProperty("field")
    String field;

    @JsonProperty("version")
    Integer version;

    @JsonProperty("default")
    Object myDefault;

    @JsonProperty("parameters")
    TopicKeyParameters parameters;
}
