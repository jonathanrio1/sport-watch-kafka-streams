package fr.samsacorp.kafka.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BeforeAfterFields {

    @JsonUnwrapped
    ConnectedWatchFields connectedWatchFields;

    @JsonUnwrapped
    MemberFields memberFields;

}
