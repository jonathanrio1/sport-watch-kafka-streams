package fr.samsacorp.kafka.config;

import fr.samsacorp.kafka.exception.StreamsCustomUncaughtExceptionHandler;
import fr.samsacorp.kafka.exception.StreamsDeserializationErrorHandler;
import fr.samsacorp.kafka.exception.StreamsRecordProducerErrorHandler;
import fr.samsacorp.kafka.util.KeyWrapperSerde;
import fr.samsacorp.kafka.util.ValueWrapperSerde;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.config.StreamsBuilderFactoryBeanConfigurer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@EnableKafkaStreams
@Configuration
public class KafkaConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServer;

    @Value("${spring.kafka.streams.application-id}")
    private String applicationId;

    @Value("${kafka.topic.dlq}")
    private String dlqTopic;

    @Value("${kafka.max-retry}")
    private int maxRetries;

    private static final String PROP_TOPIC_ERROR = "topic.dlq";
    private static final String PROP_KAFKA_TEMPLATE = "kafka.template";


    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(props));
    }

    @Bean
    StreamsBuilderFactoryBeanConfigurer streamsBuilderFactoryBeanConfigurer() {
        return factoryBean -> {
            Objects.requireNonNull(factoryBean.getStreamsConfiguration()).put(PROP_TOPIC_ERROR, dlqTopic);
            factoryBean.getStreamsConfiguration().put(PROP_KAFKA_TEMPLATE, kafkaTemplate());
            factoryBean.getStreamsConfiguration().put(
                    StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, StreamsDeserializationErrorHandler.class);
            factoryBean.getStreamsConfiguration().put(
                    StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG, StreamsRecordProducerErrorHandler.class);
            factoryBean.setKafkaStreamsCustomizer(kafkaStreams ->
                    kafkaStreams.setUncaughtExceptionHandler(
                            new StreamsCustomUncaughtExceptionHandler(kafkaTemplate(), dlqTopic, maxRetries)));
        };
    }

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public KafkaStreamsConfiguration kStreamsConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, KeyWrapperSerde.class.getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, ValueWrapperSerde.class.getName());
        return new KafkaStreamsConfiguration(props);
    }
}
