package fr.samsacorp.kafka.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.errors.DeserializationExceptionHandler;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Map;

@Slf4j
public class StreamsDeserializationErrorHandler implements DeserializationExceptionHandler {

    private KafkaTemplate<String, String> kafkaTemplate;
    private String dlqTopic;

    @Override
    public DeserializationHandlerResponse handle(ProcessorContext context,
                                                 ConsumerRecord<byte[], byte[]> consumerRecord,
                                                 Exception exception) {

        log.warn("Error while deserialization, sending to DLQ '{}' : taskId = '{}', origin_topic = '{}', partition = '{}', offset = '{}', key = '{}', value = '{}' -> {}",
                dlqTopic, context.taskId(), consumerRecord.topic(), consumerRecord.partition(), consumerRecord.offset(), new String(consumerRecord.key()), new String(consumerRecord.value()), exception.getMessage());
        kafkaTemplate.send(dlqTopic, new String(consumerRecord.key()), new String(consumerRecord.value()));
        return DeserializationHandlerResponse.CONTINUE;
    }

    @Override
    public void configure(Map<String, ?> map) {
        kafkaTemplate = (KafkaTemplate<String, String>) map.get("kafka.template");
        dlqTopic = (String) map.get("topic.dlq");
    }
}
