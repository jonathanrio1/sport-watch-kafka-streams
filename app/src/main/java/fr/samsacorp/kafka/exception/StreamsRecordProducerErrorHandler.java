package fr.samsacorp.kafka.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.RecordTooLargeException;
import org.apache.kafka.streams.errors.ProductionExceptionHandler;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Map;

@Slf4j
public class StreamsRecordProducerErrorHandler implements ProductionExceptionHandler {

    private KafkaTemplate<String, String> kafkaTemplate;
    private String dlqTopic;

    @Override
    public ProductionExceptionHandlerResponse handle(ProducerRecord<byte[], byte[]> producerRecord, Exception exception) {

        log.warn("Error while producing the record : origin_topic = '{}', partition = '{}', key = '{}', value = '{}' -> {}",
                producerRecord.topic(), producerRecord.partition(), new String(producerRecord.key()), new String(producerRecord.value()), exception.getMessage());

        kafkaTemplate.send(dlqTopic, new String(producerRecord.key()), new String(producerRecord.value()));
        if (exception instanceof RecordTooLargeException) {
            return ProductionExceptionHandlerResponse.CONTINUE;
        }
        return ProductionExceptionHandlerResponse.FAIL;
    }

    @Override
    public void configure(Map<String, ?> map) {
        kafkaTemplate = (KafkaTemplate<String, String>) map.get("kafka.template");
        dlqTopic = (String) map.get("topic.dlq");
    }
}
