package fr.samsacorp.kafka.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.errors.StreamsException;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.springframework.kafka.core.KafkaTemplate;


@Slf4j
public class StreamsCustomUncaughtExceptionHandler implements StreamsUncaughtExceptionHandler {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String dlqTopic;
    private final int maxRetries;

    public StreamsCustomUncaughtExceptionHandler(KafkaTemplate<String, String> kafkaTemplate, String dlqTopic,
                                                 int maxRetries) {
        this.kafkaTemplate = kafkaTemplate;
        this.dlqTopic = dlqTopic;
        this.maxRetries = maxRetries;
    }

    private static final String NP_EXCEPTION = "NullPointerException";
    private int currentFailureCount;

    @Override
    public StreamThreadExceptionResponse handle(Throwable exception) {

        currentFailureCount++;

        if ( (exception instanceof StreamsException && exception.getCause().toString().contains(NP_EXCEPTION)) ||
                currentFailureCount >= maxRetries) {
            log.error("Error while streaming the record, shutdown client : {}", exception.getMessage());
            kafkaTemplate.send(dlqTopic, exception.getMessage());
            return StreamThreadExceptionResponse.SHUTDOWN_CLIENT;
        }

        log.warn("Error while streaming the record, replace thread with retry {}/{}: {}",
                currentFailureCount, maxRetries, exception.getMessage());
        return StreamThreadExceptionResponse.REPLACE_THREAD;
    }
}
