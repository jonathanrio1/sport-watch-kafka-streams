package fr.samsacorp.service;

import fr.samsacorp.entity.MembersEntity;
import fr.samsacorp.entity.RiskRatesEntity;
import fr.samsacorp.repository.MembersRepository;
import fr.samsacorp.repository.RiskRatesRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
@Slf4j
public class SportService {

    RiskRatesRepository riskRatesRepository;
    MembersRepository membersRepository;
    CacheManager cacheManager;


    @Cacheable("risk_rates")
    public RiskRatesEntity getPriceByRisk(int risk) {
        RiskRatesEntity riskRates = riskRatesRepository.findByRisk(risk).orElse(null);
        if (riskRates == null) {
            throw new NoSuchElementException(String.format("No risk rate found for criteria : risk = '%s'", risk));
        }
        return riskRates;
    }

    public MembersEntity getMemberById(int id) {
        MembersEntity members = membersRepository.findById(id).orElse(null);
        if (members == null) {
            throw new NoSuchElementException(String.format("No member found for criteria : id = '%s'", id));
        }
        return members;
    }

    @Scheduled(fixedRate = 360000)
    public void refreshAllcachesAtIntervals() {
        refreshAllCaches();
    }

    private void refreshAllCaches() {
        cacheManager.getCacheNames().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }
}
