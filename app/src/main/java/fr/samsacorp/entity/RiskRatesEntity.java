package fr.samsacorp.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Table(name = "risk_rates", schema = "samsacorp")
@Data
public class RiskRatesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private int id;

    @NotNull
    private int risk;

    @NotNull
    private Double price;

}
