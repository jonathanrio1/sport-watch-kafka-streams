package fr.samsacorp.repository;

import fr.samsacorp.entity.RiskRatesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RiskRatesRepository extends JpaRepository<RiskRatesEntity, Long> {

    @Query("SELECT r FROM RiskRatesEntity r WHERE r.risk = :risk")
    Optional<RiskRatesEntity> findByRisk(@Param("risk") int risk);

}
