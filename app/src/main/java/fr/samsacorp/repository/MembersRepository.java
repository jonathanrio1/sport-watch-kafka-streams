package fr.samsacorp.repository;

import fr.samsacorp.entity.MembersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface MembersRepository extends JpaRepository<MembersEntity, Long> {

    @Query("SELECT m FROM MembersEntity m WHERE m.id = :id")
    Optional<MembersEntity> findById(@Param("id") int id);

}
